// Vue router configuration file

import Vue from 'vue'               // Access the Vue framework
import VueRouter from 'vue-router'  // Access the Vue router functionality
import ProductDetail from '@/views/ProductDetail'
// @ is shorthand for the application root folder
import Products from '@/views/Products' // Access the router view called products.vue in the views folder


Vue.use(VueRouter)    // Tell Vue we are using the Vue router

const routes = [      // Hold the path-router view association - like request mapping controllers
  {                   // each path-router view association is in it's own object
    path: '/',        // associate the ath '/' to a router view
    name: 'products', // name for this path/router view association
    component: Products // name of the router view component in the views folder
  },
  {  // a path with a value in it is referred to as a "dynamic path"
    path: '/product/:id',   // associate this path with a router view
    name: 'product-detail',   // name for this path/router view association ** may need an s here **
    component: ProductDetail  // name of the router view component in the views folder
  }
  //{
  //  path: '/product/:id/add-review',
  //  name: 'add-review',
  //  component: AddReview
 // }
]

const router = new VueRouter({  // instantiate a Vue router object so we can use Vue router
  mode: 'history',              // Use history mode - make it look to the user as if qe were using different pages
                                // put the path in the user address bar and enable th back button
  base: process.env.BASE_URL,   // Get the BASE_URL for the server from the env file 
  routes                        // Name of the array with the path-router view associations
})

export default router           // expose the Vue router to external processes like Vue
