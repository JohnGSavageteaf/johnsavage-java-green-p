package com.techelevator.auctions.DAO;

import com.techelevator.auctions.model.Auction;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
//@RequestMapping BEFORE the Controller class
// Set the default base path for all aths specified in other @RequestMapping
//
// For example if you want a controller for path"/auctions"    -@RequestMapping("", method=...)
//                if you want a controller for path "/auctions/{id}"     -@RequestMapping("/{id}", method=....)
public interface AuctionDAO {

    List<Auction> list();

    Auction get(int id);

    Auction create(Auction auction);

    List<Auction> searchByTitle(String title_like);

    List<Auction> searchByPrice(double currentBid_lte);

    List<Auction> searchByTitleAndPrice(String title, double currentBid);
}
