package com.techelevator.reservations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
// Spring boot framework server
// set of classes to make server programing easier
// like spring dao's framework make data base access easier
//
// This is generated for you when you use the spring initializer

@SpringBootApplication
public class HotelReservationsServer {

    public static void main(String[] args) {
        SpringApplication.run(HotelReservationsServer.class, args);
    }

}
