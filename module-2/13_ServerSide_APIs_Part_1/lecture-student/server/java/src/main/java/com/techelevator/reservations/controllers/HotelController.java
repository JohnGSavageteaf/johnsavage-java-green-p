package com.techelevator.reservations.controllers; // controllers have minimal logic get a list return the list

import com.techelevator.reservations.dao.HotelDAO;
import com.techelevator.reservations.dao.MemoryHotelDAO;
import com.techelevator.reservations.dao.MemoryReservationDAO;
import com.techelevator.reservations.dao.ReservationDAO;
import com.techelevator.reservations.models.Hotel;
import com.techelevator.reservations.models.Reservation;
import org.springframework.web.bind.annotation.*;

import java.util.List;


// This is a controler for URL calls to the server
// coordinate request from client (view) to get/process data managed by the dao (model)

// springboot automatically converts JSON to Java objects (deserialize) when receiving a request
//                          and Java objects to JASON (serialize) when returning to client

@RestController // tells server there are methods in this class to handle URL paths
public class HotelController {

    // define references for any dao's used
    private HotelDAO         hotelDAO;
    private ReservationDAO   reservationDAO;
    // constructor for this class - initialize member data when object of this class is instantiated
    public HotelController() {
        this.hotelDAO = new MemoryHotelDAO();  // instantiate a new hotel dao and assign it to the reference
        this.reservationDAO = new MemoryReservationDAO(hotelDAO); // instantiate a new reservation dao and assign to reference
        // since the reservation dao need a hotel dao you send it one.
    }

    /** METHODE
     * Return All Hotels
     *
     * @return a list of all hotels in the system
     */
    // A controller method to handle a URL path to the server
    // @ request mapping = Annotation to identify the path an HTTP request this method will handle
    @RequestMapping(path = "/hotels", method = RequestMethod.GET)  // handle the /hotel path for an HTTP GET req
    public List<Hotel> list() { // return a list of Hotel objects and receive no parameters
       //  return      receive
        System.out.println("Hello from the list in Controller");
        return hotelDAO.list(); // return what ever the hotelDAO sends back
    }

    /**
     * Return hotel information
     *
     * @param id the id of the hotel
     * @return all info for a given hotel
     */
    // handle an HTTP GET for the URL path /hotels/id - id is a variable in the URL path
    @RequestMapping(path = "/hotels/{id}", method = RequestMethod.GET) // {id} indicate a path variable called id is expected
    public Hotel get(@PathVariable int id) { // @path variable says get the id from the path and store it
                                            // in an int called id
        System.out.println("/hotel/ " + id + "path received from the server"); // log what was sent and how you got here
        return hotelDAO.get(id); // return whatever the hotelDAO get(id) method returns
    }

    // Write a controller to add a reservation to our reservation resource
    //      using the path: hotels/id/reservations = /hotels/1/reservations that path will add a reservation for hotel id 1
    //                                                      to our resource
    //
    // data for reservation to be added is expected to be a reservation object in the body of an HTTP POST
    // so... we need the Reservation POJO in this project - we have one!
    //  we need a method in the Reservation DAO to add a Reservation - we have one..
    //  Reservation create(Reservation reservation, int hotelID);
    //  we need to use @PathVariable to get the hotel id from the path
    //  we need to use @RequestBody to get the Reservation object out of the request body

    @RequestMapping (path="/hotels/{id}/reservations", method=RequestMethod.POST)
    //                                      path var name   pgm variable name
    public Reservation addAReservation(@PathVariable("id") int hotelId                     // get the path variable called id, store it in the int called id
                                                                                // store it in an int called hotelId
                                      ,@RequestBody Reservation aReservation  ) {// instantiate a reservation from the request body

        System.out.println("/hotel/" +hotelId+ "reservation received from server");
        // instantiate a new reservation from the one returned from the ReservationDAO
        Reservation theReservation = reservationDAO.create(aReservation, hotelId);

        return theReservation; // return the reservation from the DAO


    }

    // write a controller to return a specific reservation for a hotel
    // CANNOT DO THIS - THERE IS NO DAO METHOD TO HANDLE IT

    // write a controller to return all the reservations for a hotel
    // Reservation DAO method: List<Reservation> findByHotel(int hotelID);
    // Path: /hotels/{id}/reservations
    //      this is the same path for adding a Reservation
    //      It's ok because this time the path is for a GET
    //
    // The path HTTP request combination ust be unique within the controller

    @RequestMapping (path="/hotels/{id}/reservations", method=RequestMethod.GET)
    public List<Reservation> findByHotel(@PathVariable("id") int hotelId) {
        System.out.println("/hotel/ " + hotelId + "path received for a GET from the server");
        return reservationDAO.findByHotel(hotelId);
    }

    //Write a controller to return all reservations
    // Path: /reservations
    @RequestMapping(path="reservations", method=RequestMethod.GET)
    public List<Reservation> getAllReservations() {
        System.out.println("/reservations path received for a GET from the server");
        return  reservationDAO.findAll();
    }
}
