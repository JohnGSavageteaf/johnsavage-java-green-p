package com.techelevator.models;

public class Hotel {

    // data members mut have names that match the JSO?N attributes to be used with them
    // if we want the API interface that we use to automatically transfer the data between
    //the JSON required and use the API and this Java object
    //
    // not all Json attributes need be represented in POJO
    // we can also have data attributes not in the JSON

    private int     id;
    private String  name;
    private int     stars;
    private int     roomsAvailable;
    private String  coverImage;

    public Hotel() {  // Default constructor does nothing

    }
    // API interface will be using sandard getters and setter for this option
    // that's why we have standard getters and setters
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getStars() {
        return stars;
    }

    public int getRoomsAvailable() {
        return roomsAvailable;
    }

    public String getCoverImage() {
        return coverImage;
    }

    @Override
    public String toString() {
        return "\n--------------------------------------------" +
                "\n Hotel Details" +
                "\n--------------------------------------------" +
                "\n Id: " + id +
                "\n Name:'" + name + '\'' +
                "\n Stars: " + stars +
                "\n RoomsAvailable: " + roomsAvailable +
                "\n overImage" + coverImage;
    }
}
