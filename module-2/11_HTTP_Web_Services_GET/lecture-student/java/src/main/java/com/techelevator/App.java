package com.techelevator;

import com.techelevator.services.ConsoleService;
import com.techelevator.services.HotelService;

public class App {


    // define the constant to hold the base URL for the server (like a data source for DAO)
    private static final String API_BASE_URL = "http://localhost:3000/";

    public static void main(String[] args) {
        int menuSelection = 999;
        int hotelId = -1;

            //define instances of the services we will be using (like defining JDBC DAO object for DAO's)
        ConsoleService consoleService = new ConsoleService();
    // pass the base URL for our API server to the service that will be accessing the API
        //  (like instantiating  JDBCDAO object we pass it to the datasource)
        HotelService hotelService = new HotelService(API_BASE_URL);

        while (menuSelection != 0) {
            menuSelection = consoleService.printMainMenu(); // call the counsel service methode to display a menu
            if (menuSelection == 1) { // list all hotels option
                // Call the hotel service methode to get a list of hotels
                // then give that list to the ConsoleService methode to display it.
                consoleService.printHotels(hotelService.listHotels());
            } else if (menuSelection == 2) {
                consoleService.printReviews(hotelService.listReviews());
            } else if (menuSelection == 3) {
                System.out.println("Not implemented");
            } else if (menuSelection == 4) {
                System.out.println("Not implemented");
            } else if (menuSelection == 5) {
                System.out.println("Not implemented");
            } else if (menuSelection == 6) {
                System.out.println("Not implemented - Create a custom Web API query here");
            } else if (menuSelection == 0) {
                consoleService.exit();
            } else {
                // anything else is not valid
                System.out.println("Invalid Selection");
            }
            // Press any key to continue...
            if(hotelId != 0) {
                consoleService.next(); // call the console service to diaplay a message and wait for an answer
            }
            // Ensure loop continues
            menuSelection = 999;
        }

        // if the loop exits, so does the program
        consoleService.exit(); // call a console service method to clean up and terminate teh program
    }

}
