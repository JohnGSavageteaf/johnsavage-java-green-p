package com.techelevator.services;

import com.techelevator.models.City;
import com.techelevator.models.Hotel;
import com.techelevator.models.Review;
import org.springframework.web.client.RestTemplate;

public class HotelService {

    // RestTemplate is a framework for making and managing API calls

    private final String API_BASE_URL;  // Hold a base URL for the server
    private RestTemplate restTemplate = new RestTemplate(); // Define and instantiate a RestTemplate

    // Constructor for the class, receives  the base URL for the server and assigns our data member for holding it
    public HotelService(String apiURL) {
        API_BASE_URL = apiURL;
    }
    // return all the hotels from the API server -hotels - return all the hotels from the server
    public Hotel[] listHotels() {
    // call the API server with the hotels path and have it return a list of Hotel objects
    // getForObjects() method for the RestTemplate class receives a URL and type of Object to be returned
    // Hotel[].class  - we want an array of Hotel objects returned and Hotel is a class
    // RestTemplate AUTOMATICALLY creates objects of the class specified using the standard setters
    // it expects the data member names in the POJO to match the JSON attribute names you want
    // if they don't no data is put in the attributes of the object (nuill or 0)
        return restTemplate.getForObject(API_BASE_URL + "hotels", Hotel[].class);
    }

    // return all the reviews from the server
    public Review[] listReviews() {
        return restTemplate.getForObject(API_BASE_URL + "reviews", Review[].class);
    }
    // return one hotel by id
    public Hotel getHotelById(int id) {
        return null;
    }

    public Review[] getReviewsByHotelId(int hotelID) {
        return null;
    }

    public Hotel[] getHotelsByStarRating(int stars) {
        return null;
    }

    public City getWithCustomQuery(){
        return null;
    }

}
