--
-- Subquery Example -----------------------------------------------------------------------------------------------
--
--A subQuery is using  a select to retrieve from one table to satisfy a WHERE clause in a select
--
-- Looking up values in a table to substitue in a WHERE clause predicate. tHIS equals A SUBQUERY!
--
--Usually a subQurey is used with the WHERE IN clause = subquery may return a list of values
--                                                                             (one column in multiple rows)
--A subquery may be used with a WHERE = !- < >  if you are sure the subquery will only return one value
--
--It is safer to use an IN (rather than an =) with a subquery because it always works
--
-- You can nest up to 32,766 subqueries (more than you will ever need)
--
-- The subquery is run first and the result is plugged into the WHERE clause
--
--
--
-- Show the names of the countries that speak English
-- Be sure you are in the World Database when you run this query
--
-- The name of the country is in the country table but 
-- The country codes of the countries who speak english are in the country language talble.
-- 
-- 1. Get a list of the country codes that speak English from the country language table.
-- 2. Take the list from step 1. and use it to search the country table to get the name of the country.
--
SELECT name
   FROM country
WHERE code In(SELECT countrycode from countrylanguage WHERE language = 'English')
;        





--THIS QUERY NEEDS THE dVDsTORE DATA BASE--
-- Show any actors we have in the actor table who are not in any films
-- in the film table (ie. no entry in the film_actor table)

--
-- 1. Add some test data to the actor table we know do not
--    have matches in the film_actor table;
--
-- Who is in the actor table that is not in the film actor table?

Begin transaction; -- STARTING A REVOCERABLE UNIT OF WORK - WANT TO BE ABLE TO SAVE OR UNDO THE CHANGES

insert into actor (first_name, last_name) values('Agnes', 'Alexander');
insert into actor (first_name, last_name) values('Amber', 'Anthony');
insert into actor (first_name, last_name) values('Dana', 'Brian');
insert into actor (first_name, last_name) values('Aidan', 'Patrick'); -- Cohort 16 actor
insert into actor (first_name, last_name) values('Jared', 'Daniel');
insert into actor (first_name, last_name) values('Jess', 'Jared');
insert into actor (first_name, last_name) values('Josh', 'Lindsay');
insert into actor (first_name, last_name) values('Nia', 'Vanese');
insert into actor (first_name, last_name) values('Ruben', 'Java');
insert into actor (first_name, last_name) values('John', 'Frank');
insert into actor (first_name, last_name) values('Joiny', 'McJoinJoin');


--
--  2a. Problem solved with sub-query
--
--
-- Who is in the actor table that is not in the film actor table?
--
-- actors in the film_actor table are identified by their film_actor_id
-- actors in the actor table are identified by the actor_id
--
-- which actor_id are in the actor table but not in the film_actor table?
-- We don't nkow the actual actor_id for any of the actors
-- We have to lkook in tha actor table to find the actor_id for the actors 
--    and then see if they are in the film actor table
--
-- This is a job for a subquerry to git a list of actor_id from the actor table
-- and find which actor_id are not in the film_actor table
--

SELECT actor_id
  from actor
WHERE actor_id NOT IN (SELECT actor-id 
                        from film_actor)   -- do this first
;   

--
-- 2b. Problem solved with left-join actor table to file_actor table
--



--
-- 3. Reset table to original state before test data inserted
--
rollback; -- undo the inserts done before my test