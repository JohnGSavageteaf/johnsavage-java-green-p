---------------------------------------------------------------------------------------------------------------
--
-- DROP - remove a table and all it's data from the database
--
-- Consider referential constraints - cannot drop a parent if it has dependents
--
--      DROP TABLE table-name             - Will fail if table does not exist or if it has dependents
--
--      DROP TABLE IF EXISTS table-name   - Will run whether table exists or not; will fail if table has dependents
--
--      DROP TABLE IF EXISTS table-name  CASCADE   - Will run whether table exists or not and whether has dependents or not
--
--
--  CREATE - define a table to the database manager
--
--       CREATE TABLE table-name
--       (column-name     datßa-type    NOT NULL,
--        column-name     data-type,
--        column-name     data-type    DEFAULT   default-value,
--        column-name     data-type    UNIQUE,
--        CONSTRAINT constraint-name PRIMARY KEY (column(s)-in-table),
--        CONSTRAINT constraint-name FOREIGN KEY(for-key-column(s)) REFERENCES parent-table(pri-key-column(s)),
--        CONSTRAINT consraint-name  CHECK (where-predicate)
--       )
--
--       Note: CONTRAINT is part of the set of column definitions (i.e. inside the parens for column specification)
--
-- 
-- ALTER - changing the definition of a table
--
-- Commonly used to add/remove constraints on tables or change existing table attributes
--
--
-- Add a FOREIGN KEY - Establish Parent/Dependant relationship
--
-- FOREIGN KEY must have the same-number, same-order, compatible-data-type as entire PRIMARY KEY of parent
--             and must have a matching value in the PRIMARY KEY in the parent table
--
-- When adding FOREIGN KEY after data has been loaded into table, all FOREIGN KEY values must have a match in parent
--
--      ALTER TABLE dependent-table-name ADD FOREIGN KEY(for-key-column(s)) REFERENCES parent-table(pri-key-column(s)) 
--
-- Remove a constraint from a table
--
--      ALTER TABLE table-name DROP CONSTRAINT constraint_name
--
--
-- Add a column to an existing table
--
-- Cannot add a NOT NULL column to a table with existing data
--
--      ALTER TABLE table-name ADD COLUMN new_column_name data_type constraint
--
--
-- Rename an existing column in a table
--
--      ALTER TABLE table-name RENAME column-name TO new-column-name;
--
--
-- Rename a table
--
--      ALTER TABLE table-name RENAME TO new-table-name  - Will fail if table does not exist
--
--      ALTER TABLE IF EXISTS table-name RENAME TO new-table-name  - Successful if table exists or not
--
---------------------------------------------------------------------------------------------------------------
begin transaction;
-- Drop any existing copies of the table we are creating.
-- so we can run this entire SQL to to re-create our tables at anytime 

DROP TABLE IF EXISTS artist cascade;
DROP TABLE IF EXISTS painting cascade;
DROP TABLE IF EXISTS artist_painting cascade;

CREATE TABLE artist             -- table names are usually singular
(
artist_id serial NOT NULL     ,       -- unique id for artist
artistName character varying(50),         -- name of an artist
                                       -- a max size may be specified; if no max size 32767 is assumed
                                        -- it is more efficent if you set a maximum size.
-- add primary key to this table using the PRIMARY_KEY constraint
-- constraing name usually has the formatted with the abbgreviation of the constraint
constraint pk_artist_artist_id Primary Key (artist_id) -- tell postgres that artist Id is the primary key
)
;

-- create painting table

create table painting 
(paintingId   serial   not null,        -- data base generated unique trigger
paintingTable character varying(100) not null,   -- allows up to 100 character title
-- code a decimal or numveric type you give the total (#-digits, #-decimal place)
purchasePrice  decimal (12,2) not null,        -- maximum purchase price allowe: 1000,000,000.00 (12 digits with 2 decimal places)
constraint pk_painting_paintingId Primary Key (paintingId)
)
;

--
-- create table artist_painting
--
create table artist_painting
(
artistId        integer not null,       --these will atch colums in another table because they are foreign keys
paintingId      integer not null,       --did not use serial because we do not want another unique value--
constraint pk_artist_painting_artist_id_painting_id Primary Key (artistId, paintingId)
)
;
--
--
--Typically add FOREIGN KEYS after all the tables are createdso the odred of create tables doesn't matter
--Parent tables must be created before dependents if you don't
--
-- Use the ALTER statement to add a FOREIGN KEY constraints to the artist_painting table
-- since it is dependant on both the artist table and table.




-- Let's add a row to artist-_painting for each of our paintings by BATMAN
-- remember the data base amanger generated the artist and painting id's, so we don't know what these are.
insert into artist_painting
(artistId, paintingId) --Frank called his column artistnum, use what you called it
VALUES (
(select artistID from artist where artistName = 'BATMAN')
,(SELECT PAINTINGiD FROM PAINTING WHERE PAINTINGTITLE = 'Gotham')
)
;


rollback;