package com.techelevator.city;  // Same package as POJO and DAO Interface

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

// Concrete class for the DAO - implements the methods required by the interface
//                              and any additional methods that might be necessary

// name tells us the source of the data (JDBC), the name of the table (City) and that it's the DOA
public class JDBCCityDAO implements CityDAO {

	// define a reference to the JdbcTemplate object we will use to access Spring DAO Framework
	private JdbcTemplate jdbcTemplate;

	// constructor for the class which takes the dataSource as a parameter
	// dataSource will be provided when this DAO is instantiated (from application program)
	public JDBCCityDAO(DataSource dataSource) {
		// Instantiate a JdbcTemplate object with the dataSource give and assign it to our reference
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	// Save the given City object to the database
	@Override // Optional - Asks the compiler to be sure we implementing the method the DAO interface is expecting
	// return nothing and receives a City object
	public void save(City newCity) {
		// Define a String for the SQL statement we want to run using Spring DAO Framework
		// coding ? in the SQL statement where we want to provide values from variables
		//          when we run the statement
		// the number of ? must match the number of values expected by the SQL statement
		// Note: space after the last thing coded on the line when concatenating (+)
		// so when it's processed the lines don't run together
		String sqlInsertCity = "INSERT INTO city(id, name, countrycode, district, population) " +
							   "VALUES(?, ?, ?, ?, ?)";  // ?-indicates a value from a variable when run

		newCity.setId(getNextCityId());  // Set the id of the City object passed to
		                                 //     the next ID the database manager will assign as it's serial
		                                 //     we need to know the id of the new row in the table

		// Use our Spring DAO object to execute the SQL statement
		// .update() is used for INSERT, UPDATE, DELETE SQL statements
		//                 SQL statement, values-each-?-in-statement
		jdbcTemplate.update(sqlInsertCity, newCity.getId(),         // replace the 1st ? with the id in the City object passed
										  newCity.getName(),        // replace the 2nd ? with the name in the City object passed
										  newCity.getCountryCode(), // replace the 3rd ? with the countryCode in the City object passed
										  newCity.getDistrict(),    // replace the 4th ? with the district in the City object passed
										  newCity.getPopulation()); // replace the 5th ? with the population in the City object passed
	}

	// Return a City object from the database for the id specified
	@Override
	public City findCityById(long id) {
		City theCity = null;
		// always put a space after the last item coded in the line when using a multiline SQL statement
		// To avoid an SQL bad grammar exception due to concatenation without the space
		// this is the SQL statement to retrieve the data from the table where the id is specified
		String sqlFindCityById = "SELECT id, name, countrycode, district, population "+
							   "FROM city "+
							   "WHERE id = ?";

		// Run the SQL statement specifying values for each ? in the SQL statement
		// Since it's SELECT we use querryForRowSet()
		// an SQL row set is returned containing all the rows for the SELECT
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlFindCityById, id);
		if(results.next()) {		// if the sqlRowSet has a next row position to it in the result
			theCity = mapRowToCity(results); // copy the data from the SQLRowSet into a java object using a helper method
		}
		return theCity;
	}

	// Return all the City objects from the database for the given countryCode
	@Override
	public List<City> findCityByCountryCode(String countryCode) {
		ArrayList<City> cities = new ArrayList<>();
		String sqlFindCityByCountryCode = "SELECT id, name, countrycode, district, population "+
										   "FROM city "+
										   "WHERE countrycode = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlFindCityByCountryCode, countryCode);
		while(results.next()) {
			City theCity = mapRowToCity(results);
			cities.add(theCity);
		}
		return cities;
	}

	// Return all the City objects from the database for the given district (state)
	@Override
	public List<City> findCityByDistrict(String districtTheyWant) {
		// Define the return object for the method
		List<City> theCities = new ArrayList();
		// Define the Sql statement to be run as a string with ?'s as placeholders for values provided when it runs

		String sqlSelectByDistrict = "SELECT * " +
				"from city " +
				"where district = ?";

		// Define the SQLRowSet to hold the result from the select
		SqlRowSet theCitiesFromTheTable;

		// Run the Sql statement using my JDBC template and store the results in my SqlRowSet
		// Specify the values for any ? in the sQL statement when we run it - usually the parameter passed to the method
		theCitiesFromTheTable = jdbcTemplate.queryForRowSet(sqlSelectByDistrict, districtTheyWant);

		// Add the data from the SqlRowSet to the ArrayList we are returning as City object

		while(theCitiesFromTheTable.next()) { //Loop as lon as there are rows in the SqlRowSet
						// Next will position to the next row and return true ot return false if there are no more rows
		City aCity;
		aCity = mapRowToCity(theCitiesFromTheTable); // use the helper method to copy a row from the SqlRowSet to our new city

			theCities.add(aCity); // Add the new city with the data from the row to the new list we are returning
		}

		return theCities;  // Return the ArrayList with the Cities for the district passed into the method
	}

	// Update the City data in the database using the City object passed
	@Override
	public void update(City cityForUpdate) {
		// When receiving an object for the row to be updated, it myst ve assumed
		//
		// To update a single row in a table we need to know the value of it's primary key.
		// Since we are receiving an object that represents the row,
		//    The variable representing the primary key must be set to the value of the row to be updated
		// Also any data that will chang needs to be changed in the object
		// Any data that is not changed will also be in the object at it is now in the table
		// It is much easier to update all the values in the row using the ca;ues in the object
		// than it is to try and figure out which values should be uadted

		// All columns except the primary key are coded in the SET
		// the primary key columns are in the WHERE clause
		String SqlUpdateStatement = "UPDATE city " +
									"  SET population = ? " +
									"     , name     = ? " +
									"     , countrycode = ? " +
									"     , district     = ? " +
									"  WHERE id = ?" ;
		// Execute the update statement using the JDBCTemplate object and the value that was passed
		jdbcTemplate.update(SqlUpdateStatement, cityForUpdate.getPopulation() //get the population for City object passed for 1st ?
												, cityForUpdate.getName() //get the name for City object passed for 2nd ?
												, cityForUpdate.getCountryCode() //get the country code for City object passed for 3rd ?
												, cityForUpdate.getDistrict() //get the district for City object passed for 4th ?
												, cityForUpdate.getId() //get the id for City object passed for 5th ?
		);


	}

	// Delete the city data in the database for the given id
	@Override
	public void delete(long id) {
		// Define the SQL Delete statement
		String SqlDeleteStmt = "Delete from City where id = ?;";
		jdbcTemplate.update(SqlDeleteStmt, id);
	}

	//--------------------------------------------------------------------------------------------------------------
	//  Helper methods (not required by DAO interface) - do some common processing we need to get done
	//--------------------------------------------------------------------------------------------------------------

	// Get the next City id from the database manager
	//     because is is defined as a serial value - meaning the database manager will generate a unique integer
	// Since we need to know what id the database is going generate for the new row because we want to store in the
	//       City object passed to us - we need to retrieve from the database manager before do the INSERT
	//
	// PostgreSQL uses sequence objects to keep track of serial values
	// we can ask the sequence object to give us it's next value by SELECTing nextval('seq-object-name')
	private long getNextCityId() {
		// a SELECT SQL statement is expected to return a result of 0 or more rows
		// We need store to the result the SELECT in an SqlRowSet object when using the Spring DAO Framework


		// get the next value from sequence object called seq_city_id
		// and store it in an SqlRowSet object called nextIdResult
		// we use the .queryForRowSet() when running a SELECT because it returns a RowSet
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_city_id')");


		if(nextIdResult.next()) {              // if there is a row in the SqlRowSet object
			return nextIdResult.getLong(1);    // get the serial value from it as a Long and return it- getLong(1) - get the first column as a Long
		} else {                               // if there is NO row in the SqlRowObject (cuz we are expecting one)
			throw new RuntimeException("Something went wrong while getting an id for the new city");  // Throw an exception
		}
	}

	// Return a City object from the data retrieved from the database
	// since Java needs objects and the data from SQL is SQLRowSet we typically go to methode to create a Java object from an SqlRowSet it's name can be anything
	// commonly  call this  map something because you are mapping the SQL data to an object.
	// an SqlRowSet us the set of rows returned from a query (SELECT)
	// You must position a cursor for the SqlRowSet for the position you want to be processed
	// positioning th cursor is usually done with the next() which moves tto the next row in the SqlRowSet and returns true
	//															or returns false if the SqlRowSet is empty or you're at the end
	// When the SqlRowSet is returned by the JDBC template  methode we are positioned BEFORE the fisrt row
	// we need to do next() to get to first row(similar to using nextLine() when processing a file.

	private City mapRowToCity(SqlRowSet results) { // Receive and SQ:RowSet positioned at the row to mapped
		// This method assumes all columns are included in the SqlRowSet
		// If they are not, this method will fail

		City theCity;						// return a City object  DEFINE a reference to hold the city object
		theCity = new City();			    // instantiating a city object and assign it to the reference OR
		// City theCity = new City();	    // Define reference , instantiate and assign in one statement

		// Set the attributes in the newCity object from the values in the SqlRowSet using the object setters
		theCity.setId(results.getLong("id"));	//get the value for id from the SqlRowSet and assign it to the City object
		theCity.setName(results.getString("name")); //get the value for name from the SqlRowSet and assign it to the City object
		theCity.setCountryCode(results.getString("countrycode")); //get the value for countrycode from the SqlRowSet and assign it to the City object
		theCity.setDistrict(results.getString("district")); //get the value for district from the SqlRowSet and assign it to the City object
		theCity.setPopulation(results.getInt("population"));//get the value for population from the SqlRowSet and assign it to the City object

		return theCity; // return the City object with the data from the SqlRowSet
	}
}
