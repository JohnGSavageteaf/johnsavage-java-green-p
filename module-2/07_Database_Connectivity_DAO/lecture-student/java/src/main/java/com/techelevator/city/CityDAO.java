package com.techelevator.city; // same package as the POJO

// DAO interface for the tables yo want to access

import java.util.List;

// If one wantrs to be known as a CityDAO it MUST implement these methods
public interface CityDAO {

	// THINK IN OBJECTS !!!!
	//NOTICE OBJECTS ARE USED IN THE DAO'S INSTEAD OF INDIVIDUAL VARIABLES
	// EXCEPT FOR THE DELETE AND THE FIND
	//WHEN SAVING OF UPDATING DATA TO THE DATABASE - OBJECTS ARE USED NOT VARIABLES

	// Save the given City object to the database
	public void save(City newCity);

	// Return a City object from the database for the id specified
	public City findCityById(long id);

	// Return all the City objects from the database for the given countryCode
	public List<City> findCityByCountryCode(String countryCode);

	// Return all the City objects from the database for the given district (state)
	public List<City> findCityByDistrict(String district);

	// Update the City data in the database using the City object passed
	public void update(City city);

	// Delete the city data in the database for the given id
	public void delete(long id);
}
