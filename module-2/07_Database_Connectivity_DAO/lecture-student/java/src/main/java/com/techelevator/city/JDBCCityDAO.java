package com.techelevator.city;  // Same package as the POJO and DAO interface

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
	// Concrete class for the DAO - implements the methods required by the interface
	// and any additional methods that might be necessary
	// all the work and brains

	// name tells us the source of the data (JDBC), the name of the city  and that it's the DAO
public class JDBCCityDAO implements CityDAO {
	// Define a reference to the Jdbc Template object that we will use to access Spring DAO framework
	private JdbcTemplate jdbcTemplate;
	// Constructor for the class that takes the dataSource as a parameter
	// dataSource will be provided when this DAO is instantiated (from
	public JDBCCityDAO(DataSource dataSource) {
		// instantiate a JDBCTemplate object with the datasource we are given
		// and assign it to our reference
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	// Save the given City object to the database
	@Override // Optional - Asks the compiler to be sure we are implementing  the method the DAO interface is expecting
	// return nothing and receive a City object
	public void save(City newCity) {
		// define a string for the sql statement we want to run usign the spring DAO frameowrk
		// coding ?'s in the sql statement where we want to define data usually from variable.
		// 		when we run the statement
		// number of '?''s must match the number of values expected by the statement.
		String sqlInsertCity = "INSERT INTO city(id, name, countrycode, district, population) " +
							   "VALUES(?, ?, ?, ?, ?)"; // '?' indicates a value from a variable

		newCity.setId(getNextCityId()); // set Id of city object path to
										// the next id the database manager will assign
										// we need to know the next id of teh new row in the table

		// use our spring DAO object ot execute the sql statement
		// ().update method is used for  INSERT, UPDATE and DELETE statements
		//					SQL statement, values-each-?-in-statement
		jdbcTemplate.update(sqlInsertCity, newCity.getId(), // replace 1st ? with teh id in the city object passed
										  newCity.getName(), // replace 2nd ? with name passed
										  newCity.getCountryCode(), // replace 3rd ? with ...
										  newCity.getDistrict(),
										  newCity.getPopulation());
	}

	// Return a City object from the database for the id specified
	@Override
	public City findCityById(long id) {
		City theCity = null;
		String sqlFindCityById = "SELECT id, name, countrycode, district, population "+
							   "FROM city "+
							   "WHERE id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlFindCityById, id);
		if(results.next()) {
			theCity = mapRowToCity(results);
		}
		return theCity;
	}

	// Return all the City objects from the database for the given countryCode
	@Override
	public List<City> findCityByCountryCode(String countryCode) {
		ArrayList<City> cities = new ArrayList<>();
		String sqlFindCityByCountryCode = "SELECT id, name, countrycode, district, population "+
										   "FROM city "+
										   "WHERE countrycode = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlFindCityByCountryCode, countryCode);
		while(results.next()) {
			City theCity = mapRowToCity(results);
			cities.add(theCity);
		}
		return cities;
	}

	// Return all the City objects from the database for the given district (state)
	@Override
	public List<City> findCityByDistrict(String district) {
		// TODO Auto-generated method stub
		return null;
	}

	// Update the City data in the database using the City object passed
	@Override
	public void update(City city) {
		// TODO Auto-generated method stub
		
	}

	// Delete the city data in the database for the given id
	@Override
	public void delete(long id) {
		// TODO Auto-generated method stub
		
	}

	// -----------------------------------------------------------------
	//Helper methods (not required by the DAO interface) - do some common processing we need to get done
	//------------------------------------------------------------------

	// Get the next City id from the database manager
	// because id is defined by a serial value - meaning the database manager will generate a unique intiger
	// since we need to know which id the database manager is going to generate for the new row
	// because we want to store it in the City object passed to us - we need to retrieve from the database manager
	// before we do the INSERT
	// Postgres uses sequence objects to keep track of the serial values
	// we can ask the sequence object to give is it's next value by SELECTing nextval('seq-object-name

		private long getNextCityId() {
		// a SELECT SQL statement is expected to return a result of 0 or more rows
			// we need to store the result of the select in an sqlRowSet object using the spring DAO framework


		//	get the next value from sequence object called seq_city_id
		//and store it in an SqlRowSet object called nextResult
		//we use the .queryForRowSet(0 when running SELECT because it returns a RowSet
		SqlRowSet nextIdResult = jdbcTemplate.queryForRowSet("SELECT nextval('seq_city_id')");

		if(nextIdResult.next()) { // if there is a row in the SqlRowSet object
			return nextIdResult.getLong(1); //get the serial from it as a long and return it getLong(1) - get first column as a long
		} else { // if there is no rows in the SqlRow object (cause we are expecting one)
			throw new RuntimeException("Something went wrong while getting an id for the new city"); //throw an exception
		}
	}

	// Return a City object from the data retrieved from the database
	private City mapRowToCity(SqlRowSet results) {
		City theCity;
		theCity = new City();
		theCity.setId(results.getLong("id"));
		theCity.setName(results.getString("name"));
		theCity.setCountryCode(results.getString("countrycode"));
		theCity.setDistrict(results.getString("district"));
		theCity.setPopulation(results.getInt("population"));
		return theCity;
	}
}
