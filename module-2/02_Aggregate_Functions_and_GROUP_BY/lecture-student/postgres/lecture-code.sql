--------------------------------------------------------------------------------------------------------
-- Ordering, Grouping Basic Functions Lecture Code
--------------------------------------------------------------------------------------------------------
-- The order of the rows in a result is unpredictable unless you code an ORDER BY
-- You can run the same SE$LECT without an ORDER By 1000 times and ger results in the same order each time
-- but there is no guarantee the order will be the same the 1001st time.

--
-- If yo care about the order of the rows ina result, code and ORDER BY.
--
-- ORDER BY is always coded last in standard SQL.

-- ORDER BY -  Sequence of Rows in Result
--
--    ORDER BY          -- Ascending Sequence (low-high)
--    ORDER BY ASC      -- Ascending Sequence (low-high)
--    ORDER BY DESC     -- Descending Sequence (high-low)

-- Show Populations of all countries in acscending order
SELECT population --columns to include 
FROM country -- table where the columns come from in the result
ORDER BY population ASC -- see the columns in the result in ascending order.
;
-- Show Populations of all countries in acscending order
SELECT population --columns to include 
FROM country -- table where the columns come from in the result
ORDER BY population -- Ascending is assumedsee the columns in the result in ascending order.
;

-- Show Populations of all countries in descending order
SELECT population
From country
ORDER BY population DESC
;

-- Show  the names of countries and continents in ascending order
SELECT name, continent
        FROM country
        ORDER BY continent
        ;

SELECT name, continent
        FROM country
        ORDER BY continent, name
        ;        
-- Show the names of the coubntries and continents with both in ascending order        
SELECT name, continent
        FROM country
        ORDER BY name, continent
        ;            

-- Show the names of the coubntries and continents with name in descending order and continents in ascending order
SELECT name, continent
        FROM country
        ORDER BY continent, name DESC
        ;        
    
    -- Show the names of the coubntries and continents with both in descending order
SELECT name, continent
        FROM country
        ORDER BY continent DESC, name DESC
        ;        
        
        
-- Show Populations of all countries in ascending order 
--only if population is greater than 1 million
-- imclude the name of the country
SELECT name, population
From country
WHERE population > 1000000 -- shopw pnly if population is greater than 1000000
ORDER BY population
;

SELECT name, population
From country
WHERE population >= 1000000
  AND population <= 10000000 -- shopw pnly if population is between 1000000 and 100000000
ORDER BY population
;

SELECT name, population
From country
WHERE population between 1000000 AND 10000000 -- shopw pnly if population is between 1000000 and 10000000
ORDER BY population
;


--------------------------------------------------------------------------------------------------------
-- Limiting the number of rows in the result
--
-- LIMIT is proprieitary to postgreSQL - it may not work in other dialects of SQL for other data base managers
-- LIMIT n   - Limit the number of rows in the result - always goes at thE end of the SELECT
-- even after the ORDER BY
--
--
-- Show the name and average life expectancy of the countries with the 10 highest life expectancies.
SELECT name, lifeexpectancy  -- Columns to see
FROM country                 -- Table with columns   
WHERE lifeexpectancy IS NOT NULL --Only include rows of life expectancies that are not numm
ORDER BY lifeexpectancy DESC    -- See them from high to low order
LIMIT 10                        -- Limit the result to 10 rows, instead of all the rows that match the where clause
;

--------------------------------------------------------------------------------------------------------
-- Concatenating values (like the + in java with strings)
--
-- the concat operator (||) may be used to concatenate character (string) values in a result
--

-- Show the name & state in the format: "city-name, state"
-- of all cities in California, Oregon, or Washington.
-- sorted by state then city
SELECT name || ', ' || district  -- concantinate the city name a comma space and districe for my column
        as City_State -- give a name to the derived column.
FROM city
WHERE district = 'California' 
        or district = 'Oregon' 
        or district = 'Washington'
ORDER BY district, name
;

SELECT name || ', ' || district -- concantinate the city name a comma space and districe for my column
        as City_State
FROM city
WHERE district in('California', 'Oregon','Washington') -- in is an alternative to a series of ==/or conditions.
ORDER BY district, name
;


--------------------------------------------------------------------------------------------------------
-- Aggregate functions - produce one row in result for each group specified no matter how many rows are in the result
--                      rather than one row in the result for each row that satisfies the WHERE clause
-- Aggregate functions are used to produce a single value from a group of rows in a result
--
--
-- The group used by the aggregate functions is determined by the GROUP BY clause
-- if no GROUP BY clause is specified, the group is the set of rows in the result
--
--     AVG(column-expression)   - arithmentic average for group of non-NULL values in expression 
--     SUM(column-expression)   - arithmentic sum for group of a non-NULL values in expression 
--     MIN(column-expression)   - lowest value for group of non-NULL values in expression 
--     MAX(column-expression)   - highest value for group of non-NULL values in expression 
--     COUNT(*)                 - number of rows in the group
--     COUNT(column-expression) - number of rows for the group of non-NULL values in expression 
--
--      if requirement says "number of" - count(*)
--      if the requirement says "number of without nulls" in a column - count(column-name)
--      if requirement says "largest/biggest" - Max() or order by desc and LIMIT
--      if requirement says "Smallest/least" - Max() or order by asc and LIMIT
--      if requirement says "total/sum" - sum()
--      if requirement says "average" - avg()
--      if ther requirement says "for each" or "by each" - GROUP BY  
--
-- AVG(), SUM() may only bes used with numeric data types
-- MIN(), MAX() may be used with numeric, character, date, time datatypes
--
-- COUNT() is applied to rows (not columns)
--
--
-- Show average life expectancy in the world
SELECT lifeexpectancy -- show me the life expectancy 
FROM country          -- of each country  
;

SELECT AVG(lifeexpectancy) -- show me the average life expectancy 
FROM country          -- of each of all countries - one row in the result requardless of the number of countries  
;

-- show the average life expectancy in the world for each continent
-- ONLY columns used in the group by are allowed in the select.
SELECT continent, AVG(lifeexpectancy) -- a column used in the GROUP BY may be coded on the select.
FROM country
Group BY continent, -- perform the aggregate function for each inuque continent
;

-- Show the total population in Ohio
SELECT sum(population) -- total up all values in the column you give it
FROM city -- district is in city
WHERE district = 'Ohio'
;
-- Show the total and avarage population in Ohio - include the name of the State 
SELECT district
        , sum(population) as Total_people_living_in_Ohio
        , avg(population) as Average_population_in_Ohio -- total up all values in the column you give it
FROM city -- district is in city
WHERE district = 'Ohio'
GROUP BY district
;
-- Show the surface area of the smallest country in the world
SELECT surfacearea
FROM country
ORDER BY surfacearea
limit 1
;

SELECT MIN(surfacearea)
FROM country
;
-- Show the surface area of the smallest country in the world
SELECT name, surfacearea
FROM country
ORDER BY surfacearea
limit 1
;

SELECT name, MIN(surfacearea)
FROM country
GROUP BY name
order by MIN(surfacearea) 
limit 1
;

-- Show The 10 largest countries (by surface area) in the world

SELECT name, surfacearea
FROM country
order by surfacearea desc
limit 10
;

SELECT name, max(surfacearea)
FROM country
GROUP BY name, surfacearea
order by surfacearea desc
limit 10
;
SELECT name, max(surfacearea) as how_big_they_are
FROM country
GROUP BY name, surfacearea
order by 2 desc -- order by column 2 in the SELECT 0 used for derived column without names
limit 10
; 

SELECT name, max(surfacearea) as how_big_they_are
FROM country
GROUP BY name, surfacearea
order by how_big_they_are desc -- or you can use the as name in the SELECT
limit 10
;

-- Show the number of countries who declared independence in 1991
SELECT count(*) -- count(*) is the number of rows in the result
FROM country
WHERE indepyear = 1991 -- Limit result to rows with indepyearof 1991

;


--------------------------------------------------------------------------------------------------------
-- GROUP BY  - Specify the group to which the aggregate functions apply
--
--      GROUP BY column-expression
--
-- When using a GROUP BY the SELECT is limited ot aggreggate functions or columns in the GROUP BY
--
--

-- Show the number of countries where each language is spoken, order show them from most countries to least



-- Show the average life expectancy of each continent ordered from highest to lowest



-- Exclude Antarctica from consideration for average life expectancy



-- What is the sum of the population of cities in each state in the USA ordered by state name



-- What is the average population of cities in each state in the USA ordered by state name


--------------------------------------------------------------------------------------------------------
-- SUBQUERIES - Using the result from one query (inner query) in another query (outer query)
--
-- Frequently used in a WHERE clause with an IN predicate:
--
--       WHERE column-name IN (SELECT column-name FROM some-table WHERE some-predicate)
--
-- Any WHERE predicate may be used to connect the subquery in a WHERE clause, but you must
-- be sure a single value is returned from the subquery. 
--
-- Subqueries may also be used in a SELECT as a column-specification or a FROM as a table
-- (These are advanced concepts we will discuss later, if there is time)
--
-- Show the cities under the same given government leader


-- Show countries with the same independece year


-- Show the cities cities whose country has not yet declared independence yet


--------------------------------------------------------------------------------------------------------
--
-- Additional samples
--
-- You may alias column and table names to provide more descriptive names
--
SELECT name AS CityName 
  FROM city AS cities

-- Ordering allows columns to be displayed in ascending order, or descending order (Look at Arlington)
SELECT name
     , population 
  FROM city 
 WHERE countryCode='USA' 
 ORDER BY name ASC, population DESC
;
-- Limiting results allows rows to be returned in 'limited' clusters where LIMIT says how many, 
-- and an optional OFFSET specifies number of rows to skip
SELECT name
     , population 
  FROM city 
  LIMIT 10 OFFSET 10
;