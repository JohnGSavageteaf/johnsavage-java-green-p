package com.techelevator;

public class FranksArrayExample {
	
	public static void main(String[] args) {  
		
		System.out.println("Welcome to Frank's Array Example Program");

		int score1 = 10;
		int score2 = 20;
		int score3 = 30;

		int score4 = 40; // added after coded.
		int score5 = 50;


		int sum = 0;  // holds sum

		double avg = 0;  // holds average

		sum = score1 + score2 + score3 + score4 + score5; // change in data requires change in code.

		avg = sum / 5;									// change in data requires change in code.

		// Tightly coupled code = a change in data requires a change in code.
		// This is a bad programming practice!  Due to Forgetting to change some code dependant on the data.
		// Possibly leading to invalid precessing.

		System.out.println("Sum of scores " + sum);
		System.out.println("average "  + avg);
	//*************************************************************************************************************

		// use an array to hold and process our scores - make the code loosely coupled - (not tied to the data).
		//
		// code is not directly tied to the data - changes to data does not require change the code.
		// sign of a professional programmer.

		// define and empty array to hold scores and put values in later.

//		int[] myScores = new int[5]; // define an empty array of 3 int's - changed to 5 after program was written
//
//		myScores[0] = 10;  //set 1st element in array to 10
//		myScores[1] = 20;  // set 2nd element array to 20.
//		myScores[2] = 30;  // set 3rd element array to 30.
//		myScores[3] = 40;	// set 4th element array to 40. after coding was done
//		myScores[4] = 50;	// set 5th element array to 50. after coding was done

		// Define an array and initialize it to known value at the same time
		// Code the values inside{} separated by commas instead of new datatype[number-elements]
		int[] myScores = {10,20,30, 59, 63, 5}; // Define and initialize an array - Java figures out the number of elements

		// myScores.Length - the number of elements in the array (3)
		// myScores.Length - 1 = the largest valid index (2)

		// use a for-loop to process and array from the beginning to the end.
		// a for-loop has 3-parts: for (initialization; condition; increment-part)
		//
		// initialization - done once at start of the process
		// condition part is checked before each loop - controls how many times the loop is executed
		// increment is done at the end of the loop body (before it goes back to check the condition)
		//
		// a for-loop will execute the statements in the loop body as long as the condition is true
		//
		//When processing an array: initialization - set Loop-index to 0
		//							condition - Loop as long as the index is inside the array
		//							increment - part add 1 to the index
		// for (int i=0; i < arrayName.Length; i++)
		// 		int i=0 define and set loop index to 0 - start at first element in the array
		// 		i < arrayName.Length - keep the index inside the array (max value for i is Length -1)
		//		i ++ - increment i (ass to Loop index) - i = i+1, or i = i += ok too

		int total = 0;  // hold the sum total of a;; value in the array
		double average= 0;  // hold the average of the values in the array
		// Loop through the array adding each element to the total
		// 		is 0 < 3 (i will go from 0,1,2 - when i is 3 we exit loop
		for (int i=0; i < myScores.length; i++) { // Loop through the array adding each element to total
		total = total + myScores[i];			// add current element to total	(i=0, 1, 2) as we run

		}
		average = total / myScores.length;  //

		System.out.println(" Total of the array length element is: " + total);
		System.out.println("Average of the array length element is: " + average);
		// by using an array and for loop with .Length value - code is loosely coupled
		// adding or removing elements does not require coding changes

	}   
   
}